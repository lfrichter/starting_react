import React, { FormEvent, ChangeEvent, useState } from 'react';
import './App.css';

function App() {

  const [name, setName] = useState<string>("Luis Fernando");

  function onSubmit(event: FormEvent){
      event.preventDefault();
      alert('Form submited!');
  }

  function onChange(event: ChangeEvent<HTMLInputElement>){
      setName(event.target.value);
  }


  return (
   <div>
     <h1>Iniciando com o React</h1>
     <form onSubmit={onSubmit}> 
       <p>
        <label htmlFor="name">Nome:</label> 
        <input type="text" id="name" value={name} onChange={onChange} />
       </p>
       <p>
          <br /><br /><br /><br /><br /><br />
          {name}
       </p>
       <p>
         <button type="submit">Enviar</button>
       </p>
     </form>
   </div>
  );
}

export default App;
